__author__ = 'vmurillo'
import collections
from abc import ABCMeta, abstractmethod
from datetime import datetime
import json
import logging

logger = logging.getLogger(__name__)
search_params = collections.namedtuple('search_params', ['board', 'off',
                                                         'departure_date', 'return_date'])
search_stats = collections.namedtuple('search_stats', ['start_time', 'duration'])

member_printer = lambda v: str(v) if not isinstance(v, datetime) else v.isoformat()


def tuple_printer(tup: tuple) -> str:
    return ','.join(x for x in map(lambda attr: '%s = %s' % (attr, member_printer(tup.__dict__[attr])), tup.__dict__))


class FlightSearch(metaclass=ABCMeta):
    def __init__(self, search_parameters: search_params):
        self.search_parameters = search_parameters
        self.prices = None
        self.screen_shot = None
        self.search_stats = None

    def set_search_results(self, prices: list, screen_shot: str):
        self.prices = prices
        self.screen_shot = screen_shot

    def __str__(self):
        return "Search : (%s), cheapest = %f, search_stats : (%s)" % \
               (tuple_printer(self.search_parameters),
                self.cheapest,
                tuple_printer(self.search_stats))

    @property
    def cheapest(self):
        return self.prices[0]

    def process_search(self):
        start_time = datetime.now()
        flight_list, screen_shot = self.get_flights(*self.search_parameters)
        duration = (datetime.now() - start_time).seconds
        self.set_search_results(flight_list, screen_shot)
        self.search_stats = search_stats(start_time, duration)

    @abstractmethod
    def get_flights(board_point: str, off_point: str,
                    dep_date: datetime, ret_date: datetime):
        pass

    def __call__(cls, search_parameters: search_params):
        search_instance = cls.__class__(search_parameters)
        search_instance.process_search()
        return search_instance.cheapest

    def to_json(self):
        return json.dumps(self, skipkeys=True, default=lambda obj: (obj.isoformat()
                                                                    if isinstance(obj, datetime)
                                                                    else obj.__dict__),
                          sort_keys=True, indent=4)


def apply_searcher(searcher_param: tuple) -> FlightSearch:
    search, param = searcher_param
    search_instance = search(param)
    try:
        search_instance.process_search()
    except Exception as e:
        logger.error('Error when processing search %s'%e)
        search_instance = None
    return search_instance