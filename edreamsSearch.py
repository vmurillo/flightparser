__author__ = 'vmurillo'
from datetime import *
import logging
from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from driverWrapper import SafeWebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import re
from flightSearch import FlightSearch, search_params, search_stats

logger = logging.getLogger(__name__)

EDREAMS_INPUT_DATE_FORMAT = '%d/%m/%Y'
REGEX_PRICE = re.compile('.*\s(\d+,\d+)\s.*')
PRICE_PARSER = lambda price_string:\
    float(REGEX_PRICE.match(price_string.text.replace('\xa0', '')).groups()[0].replace(',', '.'))


class EdreamsSearch(FlightSearch):
    @staticmethod
    def get_flights(board_point: str, off_point: str,
                    dep_date: datetime, ret_date: datetime):
        """
        :param board_point: IATA code of boardPoint i.e. LIM,NCE
        :param off_point: IATA code of offPoint i.e. LIM,NCE
        :param dep_date: datetime object for departure date
        :param ret_date: datetime object for return date
        :return:
        """
        logger.debug("Board=%s,Off=%s,Departure=%s,Return=%s" % (board_point, off_point,
                                                                 dep_date.isoformat(), ret_date.isoformat()))

        with SafeWebDriver(webdriver.PhantomJS(executable_path="./phantomjs")) as web_driver:

            assert isinstance(web_driver, WebDriver)

            web_driver.get("http://www.edreams.fr")
            web_driver.execute_script("document.getElementById('departureLocation').value='%s';" % board_point)
            web_driver.execute_script("document.getElementById('arrivalLocation').value='%s';" % off_point)
            web_driver.execute_script("document.getElementById('departureDate_fake').value='%s';" %
                                      dep_date.strftime(EDREAMS_INPUT_DATE_FORMAT))
            web_driver.execute_script("document.getElementById('returnDate_fake').value='%s';" %
                                      ret_date.strftime(EDREAMS_INPUT_DATE_FORMAT))
            web_driver.find_element_by_id('homeFlightsSearch').submit()

            #wait for response
            try:
                WebDriverWait(web_driver, 60).until(
                            EC.presence_of_element_located((By.ID, 'itinerary0')))
            except TimeoutError:
                logger.error("Timeout error, web site didn't answer after 1 min")
                return None

            web_driver.save_screenshot('tmp2.png')

            #parse html
            html_string = web_driver.page_source
            parsed_html = BeautifulSoup(html_string)
            all_prices = parsed_html.find_all('div', {'class': 'singleItinerayPrice'})

            try:
                flight_prices = list(map(PRICE_PARSER, all_prices))
            except (AttributeError, IndexError):
                logger.error("Error parsing prices in website %s", web_driver.current_url)
                return None

            screen_shot = web_driver.get_screenshot_as_base64()

        return flight_prices, screen_shot

if __name__ == '__main__':
    import loggingconfig
    logger.info("Starting get_flights")
    depart_date = datetime.strptime('20/12/14', '%d/%m/%y')
    return_date = datetime.strptime('20/01/15', '%d/%m/%y')
    search_demo = search_params("NCE", "LIM", depart_date, return_date)
    #get_flights(*search_demo)
    edreams_demo = EdreamsSearch(search_demo)
    edreams_demo.process_search()
    edreams_json = edreams_demo.to_json()
    logger.info("edreams_demo %s, json = %s, len = %d" % (edreams_demo,edreams_json,len(edreams_json)))