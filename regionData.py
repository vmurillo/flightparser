__author__ = 'vmurillo'
import logging
import random
logger = logging.getLogger(__name__)
REGION_DICT = {
    'SAM': ['LIM'],         #south america
    'EUR': ['NCE', 'PAR', 'MRS', 'LYS', 'BCN', 'MAD', 'ROM', 'MIL'] #europe
}


def get_region(code: str) -> str:
    """
     :param code: a code in three letters that represents the city in a region
    :return: the region that contains the city 'code', if no region is found it sends back 'code'
    """
    region_list = list(filter(lambda reg: code in REGION_DICT[reg], REGION_DICT))

    if len(region_list) == 1:
        region = region_list[0]
    elif len(region_list) > 1:
        logging.warning("This code %s is in more than one region, check REGION_DICT" % code)
        region = region_list[0]
    else:
        logging.warning("This code %s is not in any region, check REGION_DICT" % code)
        region = code

    return region

def get_code_same_region(code: str) -> str:

    region = get_region(code)
    codes_list = REGION_DICT[region]
    number_codes = len(codes_list)
    random_index = random.randint(0, number_codes - 1)
    return codes_list[random_index]
