__author__ = 'vmurillo'
import selenium.webdriver.remote.webdriver as selewebdriver
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class SafeWebDriver():

    def __init__(self, web_driver : selewebdriver):
        self.driver_instance = web_driver

    def __enter__(self) -> selewebdriver:
        return self.driver_instance

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug("Closing driver")
        self.driver_instance.quit()
        if exc_type is not None:
            logger.error("%s" % exc_tb)
            raise exc_val