__author__ = 'vmurillo'
from datetime import datetime, timedelta
import collections
import random
from regionData import get_code_same_region
from edreamsSearch import EdreamsSearch
from flightSearch import search_params, apply_searcher
import logging
from multiprocessing.pool import Pool
import os
import json


logger = logging.getLogger(__name__)

dates_randomizer = collections.namedtuple('Randomizer', ['departure_randomizer', 'duration_randomizer'])
SEARCHES_PARSERS = [EdreamsSearch]

NUMBER_CPU = os.cpu_count()

class SearchConfiguration():
    def __init__(self, departure_date: datetime, duration: int, dates_random: dates_randomizer,
                 number_of_searchs: int, board_point: str, off_point: str, region_mode: bool):
        """
        :param duration:
        :param departure_date:
        :param dates_random:
        :return:
        """
        self.departure_date = departure_date
        self.duration = duration
        self.dates_random = dates_random
        self.number_of_searchs = number_of_searchs
        self.board_point = board_point
        self.off_point = off_point
        self.region_mode = region_mode

    def apply_randomizer_dates(self) -> list:
        """
        Apply the Randomizer object to duration & departure_date. This will return a list of length
        number_of_searchs whose elements are of tuple type:
        (departure_date + date_variation , duration + duration_variation )
        date_variation is in (-departure_randomizer/2,departure_randomizer/2)
        duration_variation is in (-duration_randomizer/2,duration_randomizer/2)
        :return: List of randomized (departure_dates,duration)
        """
        departure_list = [self.departure_date] * self.number_of_searchs
        duration_list = [self.duration] * self.number_of_searchs

        departure_list_randomized = list(map(lambda x: x +
                                             timedelta(days=random.randint(-self.dates_random.departure_randomizer/2,
                                                                           self.dates_random.departure_randomizer/2))
                                             , departure_list))

        duration_list_randomized = list(map(lambda x: x + random.randint(-self.dates_random.duration_randomizer/2,
                                                                         self.dates_random.duration_randomizer/2),
                                            duration_list))

        return [(departure_list_randomized[index], duration_list_randomized[index])
                for index in range(self.number_of_searchs)]

    def get_board_off_list(self):
        """
        It gets the list of board&off points list
        if region_mode is False it will just return a list of
            length number_of_searchs with a constant element tuple(board_point,off_point)
        else it will return a list of length number_of_searchs as follows
            tuple ( code1 , code2 ), where code1 is chosen randomly from the list of codes in the same region
            as board_point and code2 is chosen randomly from the list of codes in the same region as off_point
        :return: List of tuples (code1,code2)
        """
        if not self.region_mode:
            return [(self.board_point, self.off_point)] * self.number_of_searchs
        else:
            boards_in_same_region = list(map(get_code_same_region, [self.board_point] * self.number_of_searchs))
            offs_in_same_region = list(map(get_code_same_region, [self.off_point] * self.number_of_searchs))

            return [(boards_in_same_region[index], offs_in_same_region[index])
                    for index in range(self.number_of_searchs)]

    def get_flight_searchers(self):
        """

        :return:
        """
        searchs_randomizer = lambda tmp: random.randint(0, len(SEARCHES_PARSERS) - 1)
        return [ SEARCHES_PARSERS[index] for index in map(searchs_randomizer, [0] * self.number_of_searchs)]

    def process(self) -> list:
        board_off_list = self.get_board_off_list()
        searcher_list = self.get_flight_searchers()
        dates_list = self.apply_randomizer_dates()

        search_param_list = []
        for index in range(self.number_of_searchs):
            board_off = board_off_list[index]
            departure_duration = dates_list[index]
            departure_return = departure_duration[0], departure_duration[0] + timedelta(days=departure_duration[1])
            search_param = search_params(board_off[0], board_off[1], departure_return[0], departure_return[1])
            search_param_list.append(search_param)

        search_result_list = []
        with Pool(processes=NUMBER_CPU) as pool:
            search_result_list = pool.map(apply_searcher, [(searcher_list[index], search_param_list[index]) for index in range(self.number_of_searchs)])

        return search_result_list

if __name__ == '__main__':
    import loggingconfig
    logger.info("Starting searchConfig")
    depart_date = datetime.strptime('20/12/14', '%d/%m/%y')
    duration = 30
    randomizer = dates_randomizer(8, 10)
    number_searchs = 20
    board = 'NCE'
    off = 'LIM'
    searcher_configuration = SearchConfiguration(depart_date, duration, randomizer, number_searchs, board, off, True)
    search_list = searcher_configuration.process()
    logger.info('\n'.join(str(search) for search in search_list))
#    logger.info('\n'.join(json.dumps(search) for search in search_list))